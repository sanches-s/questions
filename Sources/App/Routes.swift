import Vapor
import MySQLDriver
import FluentProvider

extension Droplet {
    func setupRoutes(_ mysqlDriver: MySQLDriver.Driver? = nil) throws {
        get("hello") { req in
            var json = JSON()
            try json.set("hello", "world")
            return json
        }

        get("v1/addTeam") { req in
            
            guard let teamName = req.data["teamName"]?.string else {
                return "Bad request"
            }
            
            let team = Team(name: teamName, game: "")
            try team.save()
            //print(team) // the newly saved pet's id
            let result = team.id
            
            return "\(String(describing: result))"
        }

        get("v1/teams") {_ in
            var json = JSON()
            let allTeams = try Team.all()
            try json.set("count", allTeams.count)
            try json.set("teams", allTeams)
 
            return json

        }
        
        get("v1/dropAllTeam") { _ in
            for team in try Team.all() {
                try team.delete()
            }
            
            return  try "\(Team.all().count)"
        }
        // response to requests to /info domain
        // with a description of the request
        get("info") { req in
            return req.description
        }

        get("description") { req in return req.description }
        
        
        try resource("posts", PostController.self)
    }
    
}

final class Team: Model {
    
    var name: String
    var game: String
    let storage = Storage()
    
    
    init(name: String, game: String) {
        self.name = name
        self.game = game
    }
    
    init(row: Row) throws {
        name = try row.get("name")
        game = try row.get("game")
    }
    
    func makeRow() throws -> Row {
        var row = Row()
        try row.set("name", name)
        try row.set("game", game)
        return row
    }
    
}

extension Team: Preparation {
    static func prepare(_ database: Database) throws {
        try database.create(self) { team in
            team.id()
            team.string("name")
            team.string("game")
        }
    }
    
    static func revert(_ database: Database) throws {
        try database.delete(self)
    }
}

extension Team: JSONRepresentable {
    func makeJSON() throws -> JSON {
        var json = JSON()
        try json.set("id", id)
        try json.set("name", name)
        try json.set("game", game)
        return json
    }
}
