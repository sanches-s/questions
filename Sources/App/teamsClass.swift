//
//  teamsClass.swift
//  Questions
//
//  Created by Александр Сучков on 16.09.17.
//
//

import Vapor
import FluentProvider

final class Team: Model {
    
    var name: String
    var game: String
    let storage = Storage()
    

    init(name: String, game: String) {
        self.name = name
        self.game = game
    }
    
    init(row: Row) throws {
        name = try row.get("name")
        game = try row.get("game")
    }
    
    func makeRow() throws -> Row {
        var row = Row()
        try row.set("name", name)
        try row.set("game", game)
        return row
    }
    
}

extension Team: Preparation {
    static func prepare(_ database: Database) throws {
        try database.create(self) { team in
            team.id()
            team.string("name")
            team.string("game")
        }
    }
    
    static func revert(_ database: Database) throws {
        try database.delete(self)
    }
}
