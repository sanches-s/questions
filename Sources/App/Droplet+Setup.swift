@_exported import Vapor
@_exported import MySQLDriver

extension Droplet {
    public func setup(_ mysqlDriver: MySQLDriver.Driver? = nil) throws {
        try setupRoutes(mysqlDriver)
        // Do any additional droplet setup
    }
}
